# -*- mode: python ; coding: utf-8 -*-

import sys
from glob import glob
from os import path

PROJECT_PATH = 'C:\\Users\\Sean\\OneDrive\\Projects\\ADA\\north_manager'

block_cipher = None

extra_files = [
  *glob('north_manager/**/*.qml', recursive=True),
]

datas = [(path.realpath(p), path.dirname(path.relpath(p, PROJECT_PATH))) for p in extra_files]

dll_files = [
  ('include/windows/ftd2xx.dll', '.')
]

a = Analysis(['north_manager\\north_manager_gui.py'],
             pathex=['C:\\Users\\Sean\\OneDrive\\Projects\\ADA\\north_manager'],
             binaries=dll_files,
             datas=datas,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='north_manager_gui',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='north_manager_gui')
