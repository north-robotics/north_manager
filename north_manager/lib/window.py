import sys
import os
import re
import logging
from os import path
from typing import Optional, Tuple
from PyQt5.QtCore import QUrl, Qt, QSize, QTimer, QThreadPool
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication
from north_manager.lib.object import Object
from north_manager.lib.worker import Runner, Timer

logger = logging.getLogger(__name__)
SizeType = Tuple[int, int]

os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
BASE_PATH = path.dirname(path.realpath(__file__))

sys._excepthook = sys.excepthook


def exception_hook(exctype, value, traceback):
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)


sys.excepthook = exception_hook


class Window(object):
    viewPath: Optional[str] = None
    title: str = 'Window'
    size: Optional[SizeType] = None
    minimumSize: Optional[SizeType] = None
    maximumSize: Optional[SizeType] = None
    resizable: bool = True
    flags = Qt.Window

    def __init__(self, state: Optional['WindowState']=None, title: Optional[str]=None, parent: Optional['Window']=None):
        self.logger = logger.getChild(self.__class__.__name__)

        self.state = state
        self.parent = parent

        if parent is None:
            self.app = QApplication(sys.argv + ['--style', 'fusion'])
            self.app.setAttribute(Qt.AA_EnableHighDpiScaling)
        else:
            self.app = parent.app

        self.mainWindow = QQuickView()
        self.mainWindow.setTitle(title if title is not None else self.title)
        self.mainWindow.setFlags(self.flags)
        self.mainWindow.visibleChanged.connect(self.onVisibleChanged)

        if self.size is not None:
            self.mainWindow.setBaseSize(QSize(*self.size))

        if self.minimumSize is not None:
            self.mainWindow.setMinimumSize(QSize(*self.minimumSize))

        if self.maximumSize is not None:
            self.mainWindow.setMaximumSize(QSize(*self.maximumSize))

        self.context = self.mainWindow.rootContext()

        if self.state is not None:
            self.state.window = self
            self.context.setContextProperty('app', self.state)

        if self.viewPath is not None:
            self.mainWindow.setSource(QUrl.fromLocalFile(self.viewPath))

        if len(self.mainWindow.errors()) > 0:
            errors = self.mainWindow.errors()
            exceptions = []
            for err in errors:
                message = re.sub('^file:///', '', err.toString())
                print(message, file=sys.stderr)
                exceptions.append(WindowError(message))

            exceptions.reverse()
            for err in exceptions:
                raise err

        self.initTimer = QTimer(self.mainWindow)
        self.initTimer.setSingleShot(True)
        self.initTimer.timeout.connect(self.onReady)
        self.initTimer.start(0)

        self.threadpool = QThreadPool()

    @property
    def visible(self) -> bool:
        return self.mainWindow.isVisible()

    def show(self):
        self.mainWindow.show()

        if not self.resizable:
            size = self.mainWindow.size()
            self.mainWindow.setMinimumSize(size)
            self.mainWindow.setMaximumSize(size)
            self.mainWindow.setFlag(Qt.Dialog, True)

        if self.parent is None:
            return self.app.exec_()

    def onVisibleChanged(self, visible: bool) -> None:
        if visible:
            self.onShow()
        else:
            self.onClose()

    def close(self):
        self.mainWindow.close()

    def run(self):
        sys.exit(self.show())

    def parallel(self, func, *args, result=None, error=None, **kwargs):
        def finished(value):
            if result is not None:
                result(value)

        def run():
            try:
                value = func(*args, **kwargs)
                return value
            except Exception as err:
                if error is not None:
                    error(err)

        runner = Runner(run)
        runner.signals.result.connect(finished)
        self.threadpool.start(runner)

        return runner

    def timer(self, func, *args, timeout=0.0, once=False, result=None, **kwargs):
        def onResult(value):
            if result is not None:
                result(value)

        timer = Timer(func, timeout, once=once, *args, **kwargs)
        timer.signals.result.connect(onResult)

        self.threadpool.start(timer)

        return timer

    def onReady(self):
        self.logger.info(f'onReady')

        if self.state is not None and hasattr(self.state, 'onReady'):
            self.state.onReady()

    def onShow(self):
        self.logger.info('onShow')

        if self.state is not None and hasattr(self.state, 'onShow'):
            self.state.onShow()

    def onClose(self):
        self.logger.info('onClose')

        if self.state and hasattr(self.state, 'onClose'):
            self.state.onClose()


class WindowState(Object):
    window: Window = None
    parent: Object = None


class WindowError(Exception):
    pass
