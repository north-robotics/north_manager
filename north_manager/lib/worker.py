import time
from PyQt5.QtCore import QObject, QRunnable, pyqtSlot, pyqtSignal


class Signals(QObject):
    result = pyqtSignal(object)


class Worker(QRunnable):
    def __init__(self):
        QRunnable.__init__(self)
        self.signals = Signals()

    @pyqtSlot()
    def run(self):
        result = self.start()
        self.signals.result.emit(result)

    def start(self):
        pass


class Runner(Worker):
    def __init__(self, func, *args, **kwargs):
        Worker.__init__(self)
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def start(self):
        return self.func(*self.args, **self.kwargs)


class Timer(Worker):
    def __init__(self, func, timeout, *args, immediate: bool=False, once: bool=False, enabled: bool=True, **kwargs):
        Worker.__init__(self)
        self.func = func
        self.timeout = timeout
        self.immediate = immediate
        self.once = once
        self.args = args
        self.kwargs = kwargs
        self.running = False
        self.enabled = enabled

    @pyqtSlot()
    def run(self):
        first = True
        self.running = True
        while self.running:
            if self.enabled:
                if self.immediate or not first:
                    result = self.func(*self.args, **self.kwargs)
                    self.signals.result.emit(result)

                    if self.once:
                        return

                time.sleep(self.timeout)
                first = False
            else:
                time.sleep(0.1)
