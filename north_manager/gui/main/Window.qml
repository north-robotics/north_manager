import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.10
import QtQuick.Dialogs 1.0

Item {
  id: mainWindowItem
  width: 400
  height: 200

  ColumnLayout {
    anchors.fill: parent
    anchors.margins: 10
    spacing: 10

    Connection {
      Layout.fillWidth: true
    }

    GridLayout {
      Layout.fillWidth: true
      Layout.fillHeight: true
      columns: 3
      enabled: app.connected && !app.connecting

      Repeater {
        model: app.tools

        Button {
          text: modelData.name
          Layout.fillHeight: true
          Layout.fillWidth: true
          enabled: modelData.hasWindow()
          onClicked: app.onToolButtonClicked(modelData)
        }
      }
    }
  }
}