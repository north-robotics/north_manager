import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
  height: layout.height

  RowLayout {
    id: layout
    width: parent.width

    Label {
      text: "Controller"
      leftPadding: 9
    }

    ComboBox {
      id: controllersComboBox
      Layout.fillWidth: true
      model: app.controllerNames
      enabled: app.controllers.length > 0 && !app.connected
    }

    Button {
      text: app.connected && !app.connecting ? "Disconnect" : "Connect"
      enabled: app.controllers.length > 0 && !app.connecting
      onClicked: app.onConnectButtonClicked(controllersComboBox.index)
    }
  }
}