import time
import logging
from os import path
from typing import List
from ftdi_serial import Serial
from north_c9.controller import C9Controller
from north_manager.lib.window import Window, WindowState
from north_manager.lib.object import Object, computedProperty, registerQmlTypes
from north_manager.lib.api import ControllerInfo, detect_controllers
from north_manager.gui.ios.window import IoWindow, IoState
from north_manager.gui.arm.window import ArmWindow, ArmState

BASE_PATH = path.dirname(path.realpath(__file__))


class Tool(Object):
    name: str
    window: Window

    def hasWindow(self) -> bool:
        return self.window is not None


class Controller(Object):
    info: ControllerInfo
    c9Controller: C9Controller


class MainState(WindowState):
    window: Window
    tools: List[Tool] = [
        Tool(name='Update Firmware'),
        Tool(name='Control I/O', window=IoWindow(IoState())),
        Tool(name='Move Arm', window=ArmWindow(ArmState())),
        Tool(name='Control Pumps'),
        Tool(name='Read Scale'),
        Tool(name='Calibrate'),
    ]
    controllers: List[Controller] = []
    controller: Controller
    connected: bool = False
    connecting: bool = False

    controllersTimer = None

    @computedProperty('controllers')
    def controllerNames(self) -> List[str]:
        return [str(controller.info) for controller in self.controllers]

    def onReady(self):
        print('READY')
        self.controllersTimer = self.window.timer(detect_controllers, include_unknown=True, use_com=False, timeout=10, immediate=True, result=self.onControllersUpdate)

    def onControllersUpdate(self, controllers):
        print(controllers)
        self.controllers = [Controller(info=info) for info in controllers]

    def onConnectButtonClicked(self, index: int) -> None:
        print('CONNECT', index)
        self.connected = not self.connected
        self.controllersTimer.enabled = not self.connected

        if self.connected:
            self.controller = self.controllers[index]
            self.connecting = True
            self.window.parallel(self.connect, self.controller.info.port, result=self.onConnect)
        else:
            self.controller.c9Controller.disconnect()
            self.controller = None
            self.closeToolWindows()

    def connect(self, serial: str):
        return C9Controller(device_serial=serial, debug_protocol=True)

    def onConnect(self, controller: C9Controller):
        self.controller.c9Controller = controller
        print('CONNECTED', self.controller.c9Controller)
        self.connecting = False

    def onToolButtonClicked(self, tool: Tool):
        if tool.window is not None and not tool.window.visible:
            tool.window.state.parent = self
            tool.window.state.c9Controller = self.controller.c9Controller
            tool.window.show()

    def closeToolWindows(self) -> None:
        for tool in self.tools:
            if tool.window is not None and tool.window.visible:
                tool.window.close()


class MainWindow(Window):
    viewPath = path.join(BASE_PATH, 'Window.qml')
    title = 'North Robotics Manager'
    resizable = False


registerQmlTypes()

if __name__ == '__main__':
    window = MainWindow(MainState())
    window.run()
