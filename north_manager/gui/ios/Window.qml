import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.12

Item {
    anchors.fill: parent

    ColumnLayout {
        spacing: 15
        anchors.margins: 10
        anchors.fill: parent

        AnalogInputs { }

        DigitalInputs { }

        Outputs { }
    }
}