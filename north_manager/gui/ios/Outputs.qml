import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.12

GroupBox {
    title: qsTr("Outputs")
    Layout.fillWidth: true

    GridLayout {
        anchors.fill: parent
        columns: 16

        Repeater {
            model: app.outputs.length

            ColumnLayout {
                Label {
                    text: index
                    horizontalAlignment: Text.AlignHCenter
                    Layout.fillWidth: true
                }

                CheckBox {
                    id: outputCheckbox
                    padding: 0
                    Layout.fillWidth: true
                    checked: app.outputs[index]
                    onClicked: app.onOutputChanged(index, outputCheckbox.checked)
                }
            }
        }
    }
}