from os import path
from typing import List, Tuple
from north_manager.lib.window import Window, WindowState
from north_manager.lib.object import Object, registerQmlTypes
from north_c9.controller import C9Controller, C9Error

BASE_PATH = path.dirname(path.realpath(__file__))


class IoState(WindowState):
    parent: Object
    c9Controller: C9Controller
    analogInputs: List[float] = [0.0] * 8
    hasAnalogInputs: bool = True
    digitalInputs: List[bool] = [0] * 16
    outputs: List[bool] = [0] * 32

    refreshTimer = None

    def output(self, index: int) -> bool:
        print(self.outputs[0])
        output = self.outputs[index]
        return bool(self.outputs[index])

    def onReady(self):
        self.refreshTimer = self.window.timer(self.onRefresh, timeout=1, immediate=True, enabled=False)

    def onShow(self):
        self.refreshTimer.enabled = True

    def onClose(self):
        self.refreshTimer.enabled = False

    def onOutputChanged(self, index: int, state: bool):
        print('changed', index, state)
        self.window.parallel(self.c9Controller.output, index, state)

    def onRefresh(self):
        if self.c9Controller is None:
            return

        try:
            if self.hasAnalogInputs:
                try:
                    self.analogInputs = self.c9Controller.analog_inputs(all=True)
                except C9Error:
                    self.hasAnalogInputs = False

            self.digitalInputs = self.c9Controller.digital_inputs(all=True)
            self.outputs = self.c9Controller.outputs(all=True)
        except Exception as err:
            print('Error', err)


class IoWindow(Window):
    viewPath = path.join(BASE_PATH, 'Window.qml')
    title = 'C9 IO'
    minimumSize = (430, 300)
    resizable = False


registerQmlTypes()

if __name__ == '__main__':
    ioWindow = IoWindow(IoState())
    ioWindow.run()
