import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.12

GroupBox {
    title: qsTr("Analog Inputs")
    Layout.fillWidth: true
    enabled: app.hasAnalogInputs

    RowLayout {
        anchors.fill: parent
        spacing: 10

        Repeater {
            model: app.analogInputs.length

            ColumnLayout {
                Layout.fillWidth: true

                Label {
                    text: index
                    horizontalAlignment: Text.AlignHCenter
                    Layout.fillWidth: true
                }

                Label {
                    horizontalAlignment: Text.AlignHCenter
                    Layout.fillWidth: true
                    enabled: false
                    text: !app.hasAnalogInputs ? "N/A" : app.analogInputs[index].toFixed(1) + " V"
                }
            }
        }
    }
}