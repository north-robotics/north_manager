import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.12

GroupBox {
    title: qsTr("Digital Inputs")
    Layout.fillWidth: true

    RowLayout {
        anchors.fill: parent

        Repeater {
            model: app.digitalInputs.length

            ColumnLayout {
                Label {
                    text: index
                    horizontalAlignment: Text.AlignHCenter
                    Layout.fillWidth: true
                }

                CheckBox {
                    padding: 0
                    enabled: false
                    Layout.fillWidth: true
                    checked: app.digitalInputs[index]
                }
            }
        }
    }
}