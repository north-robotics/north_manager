from os import path
from typing import List, Tuple
from north_manager.lib.window import Window, WindowState
from north_c9.controller import C9Controller, C9Error
from north_manager.lib.object import Object, registerQmlTypes

BASE_PATH = path.dirname(path.realpath(__file__))


class Position(Object):
    x: str = '0'
    y: str = '0'
    z: str = '0'
    gripper: str = '0'

    def fromFloats(self, x, y, z, gripper=0):
        self.x = str(x)
        self.y = str(y)
        self.z = str(z)
        self.gripper = str(gripper)

    def toFloats(self):
        return float(self.x), float(self.y), float(self.z), float(self.gripper)


class Speed(Object):
    velocity: str = '0'
    acceleration: str = '0'

    def fromFloats(self, velocity, acceleration):
        self.velocity = str(velocity)
        self.acceleration = str(acceleration)

    def toFloats(self):
        return float(self.velocity), float(self.acceleration)


class ArmMessages:
    HOMING = 'Homing...'
    MOVING = 'Moving...'


class ArmState(WindowState):
    parent: Object
    c9Controller: C9Controller
    currentPosition: Position = Position()
    targetPosition: Position = Position()
    speed: Speed = Speed()
    moving: bool = False
    homing: bool = False
    error: bool = False
    message: str = ''

    refreshTimer = None

    def onReady(self):
        self.refreshTimer = self.window.timer(self.onRefresh, timeout=0.5, immediate=True, enabled=self.window.visible)
        self.window.parallel(self.refreshSpeed)

    def onShow(self):
        if self.refreshTimer is not None:
            self.refreshTimer.enabled = True

    def onClose(self):
        self.refreshTimer.enabled = False

    def onRefresh(self):
        if self.c9Controller is None or self.homing:
            return

        try:
            position = self.c9Controller.cartesian_position()
            self.currentPosition.fromFloats(*position)
            self.moving = True in self.c9Controller.axes_moving([0, 1, 2, 3])

            if self.moving:
                self.message = ArmMessages.MOVING
            elif not self.error:
                self.message = ''
        except Exception as err:
            print('Error', err)

    def refreshSpeed(self) -> None:
        speed = self.c9Controller.speed()
        self.speed.fromFloats(*speed)

    def onError(self, error):
        self.message = str(error)
        self.error = True
        self.window.timer(self.clearError, timeout=5, once=True)

    def clearError(self, result=None):
        self.error = False
        self.message = ''

    # movement states: onMoveClicked -> onMoveSpeedSet -> onMoveComplete
    def onMoveClicked(self) -> None:
        self.error = False
        print(self.targetPosition.x, self.targetPosition.y, self.targetPosition.z, self.targetPosition.gripper)
        self.window.parallel(self.c9Controller.speed, *self.speed.toFloats(), result=self.onMoveSpeedSet, error=self.onError)

    def onMoveSpeedSet(self, result=None) -> None:
        print('move speed set')
        self.window.parallel(self.c9Controller.move_arm, *self.targetPosition.toFloats(), result=self.onMoveComplete, error=self.onError)

    def onMoveComplete(self, result=None) -> None:
        print('move complete')

    def onHomeClicked(self) -> None:
        self.error = False
        print('home')
        self.homing = True
        self.message = ArmMessages.HOMING
        self.window.parallel(self.c9Controller.home, skip=True, result=self.onHomeComplete)

    def onHomeComplete(self, result=None) -> None:
        print('home complete')
        self.homing = False
        self.message = ''


class ArmWindow(Window):
    viewPath = path.join(BASE_PATH, 'Window.qml')
    title = 'Move Arm'
    resizable = False


registerQmlTypes()

if __name__ == '__main__':
    window = ArmWindow(ArmState())
    window.state.c9Controller = C9Controller(debug_protocol=True)
    window.run()