import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.12
import Objects 1.0 as Objects

RowLayout {
  anchors.fill: parent
  property Objects.Position position: Objects.Position { }

  Label {
    text: "X"
  }

  TextField {
    id: xTextField
    width: 10
    Layout.fillWidth: true
    text: position.x
  }

  Binding {
    target: position
    property: "x"
    value: xTextField.text
  }

  Label {
    text: "mm"
    font.italic: true
    font.pointSize: 9
    rightPadding: 5
  }

  Label {
    text: "Y"
  }

  TextField {
    id: yTextField
    width: 10
    Layout.fillWidth: true
    text: position.y
  }

  Binding {
    target: position
    property: "y"
    value: yTextField.text
  }

  Label {
    text: "mm"
    font.italic: true
    font.pointSize: 9
    rightPadding: 5
  }

  Label {
    text: "Z"
  }

  TextField {
    id: zTextField
    width: 10
    Layout.fillWidth: true
    text: position.z
  }

  Binding {
    target: position
    property: "z"
    value: zTextField.text
  }

  Label {
    text: "mm"
    font.italic: true
    font.pointSize: 9
    rightPadding: 5
  }

  Label {
    text: "Gripper"
  }

  TextField {
    id: gripperTextField
    width: 10
    Layout.fillWidth: true
    text: position.gripper
  }

  Binding {
    target: position
    property: "gripper"
    value: gripperTextField.text
  }

  Label {
    text: "degrees"
    font.italic: true
    font.pointSize: 9
    rightPadding: 5
  }
}