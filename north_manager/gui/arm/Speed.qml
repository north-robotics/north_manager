import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.12
import Objects 1.0 as Objects

RowLayout {
  anchors.fill: parent
  property Objects.Speed speed: Objects.Speed { }

  Label {
    text: "Velocity"
  }

  TextField {
    id: velocityTextField
    width: 10
    Layout.fillWidth: true
    text: speed.velocity
  }

  Binding {
    target: speed
    property: "velocity"
    value: velocityTextField.text
  }

  Label {
    text: "counts/s"
    font.italic: true
    font.pointSize: 9
    rightPadding: 5
  }

  Label {
    text: "Acceleration"
  }

  TextField {
    id: accelerationTextField
    width: 10
    Layout.fillWidth: true
    text: speed.acceleration
  }

  Binding {
    target: speed
    property: "acceleration"
    value: accelerationTextField.text
  }

  Label {
    text: "counts/s"
    font.italic: true
    font.pointSize: 9
    rightPadding: 5
  }
}