import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.12

Item {
  width: 400
  height: 300

  ColumnLayout {
    spacing: 10
    anchors.fill: parent
    anchors.margins: 10

    GroupBox {
      title: "Speed"
      Layout.fillWidth: true
      Layout.fillHeight: true

      Speed {
        speed: app.speed
      }
    }

    GroupBox {
      title: "Current Position"
      Layout.fillWidth: true
      Layout.fillHeight: true

      Position {
        enabled: false
        position: app.currentPosition
      }
    }

    GroupBox {
      title: "Target Position"
      Layout.fillWidth: true
      Layout.fillHeight: true

      Position {
        position: app.targetPosition
      }
    }

    RowLayout {
      enabled: !app.moving && !app.homing
      anchors.right: parent.right

      Label {
        text: app.message
        color: app.error ? "red" : "black"
        Layout.alignment: Qt.AlignLeft
      }

      Button {
        text: "Home"
        Layout.alignment: Qt.AlignRight
        onClicked: app.onHomeClicked()
      }

      Button {
        text: "Move"
        Layout.alignment: Qt.AlignRight
        onClicked: app.onMoveClicked()
      }
    }
  }
}