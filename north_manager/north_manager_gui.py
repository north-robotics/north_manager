import os
import sys
import logging
# add exception hook to print out uncaught exceptions from Qt thread
sys._excepthook = sys.excepthook
def exception_hook(exctype, value, traceback):
    print(exctype, value, traceback)
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)
sys.excepthook = exception_hook

# make sure we can import things using the north_manager package
sys.path += [os.path.realpath(os.path.join(__file__, '../../'))]
from north_manager.gui.main.window import MainWindow, MainState

logging.basicConfig(level=logging.DEBUG)

window = MainWindow(MainState())
window.run()
